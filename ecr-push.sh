#!/bin/bash

region=ap-northeast-1
account_id=656337613100

aws ecr get-login-password --region $region | docker login --username AWS --password-stdin $account_id.dkr.ecr.$region.amazonaws.com
docker tag $(docker image ls | grep '^ecr-push' | sed 's/  */ /g' | cut -d " " -f 3) $account_id.dkr.ecr.$region.amazonaws.com/ecr-push:latest
docker push $account_id.dkr.ecr.$region.amazonaws.com/ecr-push:latest


